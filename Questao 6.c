#include <stdio.h>
#include <stdlib.h>

int main(){
	int nota1, nota2, nota3, media;
	printf("Digite a primeira nota: \n");
	scanf("%d", &nota1);
	printf("Digite a segunda nota: \n");
	scanf("%d", &nota2);
	printf("Digite a terceira nota: \n");
	scanf("%d", &nota3);
	media=((nota1*1)+(nota2*1)+(nota3*2))/4;
	if (media>=60){
		printf("Parabens, voce foi APROVADO com media %d", media);
	} else {
		printf("Infelizmente voce foi REPROVADO com media %d", media);
	}
	return 0;
}
