#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int B;
	float A;
	printf("Digite um numero flutuante: \n");
	scanf("%f", &A);
	printf("Agora, digite um numero inteiro: \n");
	scanf("%d", &B);
	printf("A raiz entre os numeros resulta em: %.5e", pow(A, (1/B)));
	return 0;
}
