#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int A;
	float B;
	printf("Digite um numero flutuante: \n");
	scanf("%f", &A);
	printf("Agora, digite um numero inteiro: \n");
	scanf("%d", &B);
	printf("A potenciacao dos numeros resulta em: %.5e", pow(A, B));
	return 0;
}
