#include <stdio.h>
#include <stdlib.h>

int main(){
	int ano;
	printf("Digite um ano: \n");
	scanf("%d", &ano);
	
	if (ano%4==0){
		printf("\nO ano digitado e bissexto!");
	} else {
		printf("\nO ano digitado NAO e bissexto.");
	}
	return 0;
}
