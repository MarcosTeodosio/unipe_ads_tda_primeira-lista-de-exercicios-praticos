#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int numero;
	printf("Digite um numero: ");
	scanf("%d", &numero);
	if (numero%2!=0){
		printf("O numero %d eh um numero IMPAR!", numero);
	} else {
		printf("O numero %d eh um numero PAR!", numero);
	}
	return 0;
}
