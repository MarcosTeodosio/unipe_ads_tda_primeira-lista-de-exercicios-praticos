#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int codigo, contador=1;
	
	while(contador!=0){
		printf("Menu de opcoes: \n "
		"0-Opcao zero\n " //fecha o menu
		"1-Opcao um\n " //diz se � par ou �mpar
		"2-Opcao dois\n " //potenciacao de numeros
		"3-Opcao tres\n " //radiciacao de numeros
		"4-Opcao quatro\n " //ano bissexto
		"5-Opcao cinco\n " //media ponderada
		"6-Opcao seis\n " //media normal
		"7-Opcao sete\n " //LISTA 2 - QUESTAO 1 - Fatorial
		"8-Opcao oito\n " //LISTA 2 - QUESTAO 2 - Numeros primos
		"9-Opcao nove\n " //LISTA 2 - QUESTAO 3 - Fibonacci
		"10-Opcao dez\n\n" //matricula em hexadecimal
		"Digite a opcao desejada: \n");
		scanf("%d", &codigo);
		if (codigo<0 || codigo>10){
			printf("A opcao desejada e inexistente!\n\n");
		}
		if (codigo==1){
			int numero;
			printf("Digite um numero: ");
			scanf("%d", &numero);
			if (numero%2!=0){
				printf("O numero %d e um numero IMPAR!\n\n", numero);
			} else {
				printf("O numero %d e um numero PAR!\n\n", numero);
			}
		}
		if (codigo==2){
			int B;
			float A;
			printf("Digite um numero flutuante: \n");
			scanf("%f", &A);
			printf("Agora, digite um numero inteiro: \n");
			scanf("%d", &B);
			printf("A potenciacao dos numeros resulta em: %.2e\n\n", pow(A, B));
		}
		if (codigo==3){
			int B;
			float A;
			printf("Digite um numero flutuante: \n");
			scanf("%f", &A);
			printf("Agora, digite um numero inteiro: \n");
			scanf("%d", &B);
			printf("A raiz entre os numeros resulta em: %.2e\n\n", pow(A, (1.0/B)));
		}
		if (codigo==4){
			int ano;
			printf("Digite um ano: \n");
			scanf("%d", &ano);
			if (ano%4==0){
				printf("\nO ano digitado e bissexto!\n\n");
			} else {
				printf("\nO ano digitado NAO e bissexto.\n\n");
			}
		}
		if (codigo==5){
				int nota1, nota2, nota3, media;
			printf("Digite a primeira nota(0 a 100): \n");
			scanf("%d", &nota1);
			printf("Digite a segunda nota(0 a 100): \n");
			scanf("%d", &nota2);
			printf("Digite a terceira nota(0 a 100): \n");
			scanf("%d", &nota3);
			media=((nota1*1)+(nota2*1)+(nota3*2))/4;
			if (media>=60){
				printf("Parabens, voce foi APROVADO com media %d\n\n", media);
			} else {
				printf("Infelizmente voce foi REPROVADO com media %d\n\n", media);
			}
		}
		if (codigo==6){
			float nota1, nota2, media;
			printf("Digite a primeira nota(0 a 10): \n");
			scanf("%f", &nota1);
			if (nota1<0.0 || nota1>10.0){
				printf("Nota invalida!");
				exit(0);
			}
			printf("Digite a segunda nota(0 a 10): \n");
			scanf("%f", &nota2);
			if (nota2<0.0 || nota2>10.0){
				printf("Nota invalida!");
				exit(0);
			}
			media=(nota1+nota2)/2.0;	
			printf("A media do aluno foi: %.2f\n\n", media);
		}
		if (codigo==7){
			int fat, num;
			printf("Digite um numero para calcular fatorial: ");
			scanf("%d", &num);
			for(fat=1; num>1; num=num-1){
				fat=fat*num;
			}		
			printf("Resultado do fatorial: %d\n\n", fat);
		}
		if (codigo==8){
			int div, num, cont=0;
			printf("Digite um numero para saber se e primo: ");
			scanf("%d", &num);
			if(num>0){
				for(div=2; div<=num/2; div++){
					if (num%div == 0){
						cont++;
						break;
					}
				}
					if (cont==0){
						printf("O numero %d E primo.\n\n", num);
					} else if (cont!=0){
						printf("O numero %d NAO E primo.\n\n", num);
						}
			} else {
				printf("Numero invalido!\n\n");
					}
		}
		if (codigo==9){
			int x=0, y=1, num, aux, i;
			printf("Digite ate qual posicao deseja conhecer a Sequencia de Fibonacci: ");
			scanf("%d", &num);
			printf("Sequencia de Fibonacci:\n%d \n", y);
			for (i=1; i<=num; i++){
				aux = x+y;
				x = y;
				y = aux;
				printf("%d\n", aux);
			}
		}
		if (codigo==10){
			int matricula;
			printf("Digite sua matricula: \n\n");
			scanf("%d", &matricula);
			printf("Seu nome e Marcos\n");
			printf("Sua matricula em HEXADECIMAL e: %x\n\n", matricula);
		}
		if (codigo==0){
			printf("Adeus");
			exit(0);
		}
	}
}

