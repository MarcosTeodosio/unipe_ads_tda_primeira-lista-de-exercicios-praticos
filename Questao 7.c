#include <stdio.h>
#include <stdlib.h>

int main(){
	float nota1, nota2, media;
	printf("Digite a primeira nota: \n");
	scanf("%f", &nota1);
	if (nota1<0.0 || nota1>10.0){
		printf("Nota invalida!");
		exit(0);
	}
	printf("Digite a segunda nota: \n");
	scanf("%f", &nota2);
	if (nota2<0.0 || nota2>10.0){
		printf("Nota invalida!");
		exit(0);
	}
	media=(nota1+nota2)/2.0;	
	printf("A media do aluno foi: %.2f", media);
	return 0;
}
