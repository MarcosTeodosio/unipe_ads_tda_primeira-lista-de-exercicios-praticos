#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int codigo;
	do{
		printf("Menu de opcoes: \n"
		"0-Opcao zero\n "
		"1-Opcao um\n "
		"2-Opcao dois\n "
		"3-Opcao tres\n "
		"4-Opcao quatro\n "
		"5-Opcao cinco\n "
		"6-Opcao seis\n "
		"7-Opcao sete\n "
		"8-Opcao oito\n "
		"9-Opcao nove\n\n "
		"Digite a opcao desejada: \n");
		scanf("%d", &codigo);
		if (codigo<0 || codigo>9){
			printf("A opcao desejada e inexistente!\n\n");
		}
	} while(codigo!=0);
	if (codigo==0){
		printf("Adeus");
	}
	return 0;
}
